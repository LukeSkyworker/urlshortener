# README #

Сервис для генерации коротких ссылок и получения длинных ссылок из коротких.

Генерация происходит по принципу преобразования сгенерированного id из mysql в короткую строку из латинских симолов и цифр.
Всего 62 символа в алфавите.

Т.е. длинное целое число из 10-ичной системы счисления преобразуется в 62-ичную.

Сервис содержит всего 2 публичных метода:

createShortUrlFromLong - генерация и запись в базу короткой ссылки

longUrlFromShort - нахождение длинной ссылки из короткой.

Пример использования:
$urlShortenerService = new UrlShortenerService;

$shortUrl1 = $urlShortenerService->createShortUrlFromLong("http://yandex.ru");

$shortUrl2 = $urlShortenerService->createShortUrlFromLong("test2");

$shortUrl3 = $urlShortenerService->createShortUrlFromLong("http://citilink.ru/promo/x/");

$longUrl1 =   $urlShortenerService->longUrlFromShort($shortUrl1);

echo " urls: $shortUrl1 $shortUrl2 $shortUrl3";
echo " longUrl1: $longUrl1 ";