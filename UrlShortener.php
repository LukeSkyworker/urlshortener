<?php

/**
 * Сервис для генерации коротких ссылок из длинных и получения длинных ссылок из коротких
 */
class UrlShortenerService {
    
    private static $alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
     /** 
     * Генерирует и записывает в базу короткую ссылку
     * @param int $longUrl Длинный url
     *
     * @return string|false Короткий url или false, в случае неудачи
     */
    public function createShortUrlFromLong($longUrl) 
    {
        /** @todo добавить проверку на существование аналогичного длинного url */
        /** @todo добавить поле expire_date */
        mysql_query("insert into urls (url) values ($longUrl)");
        if (!filter_var($longUrl, FILTER_VALIDATE_URL)) {
            return false;
        }
        
        $urlId = mysql_insert_id();
        //$urlId = rand(100000, 1000000); // для теста без базы
        echo $urlId.' -';
        $shortUrl = $this->shortUrlFromId($urlId);
        mysql_query("update urls set short_url=$shortUrl");
        
        return $shortUrl;
    }
    
     /** 
     * Восстанавливает длинный url из короткого
     * @param int $longUrl Длинный url
     *
     * @return string|false Длинный url или false, если не найден
     */
    public function longUrlFromShort($shortUrl) 
    {
        $urlId = $this->idFromShortUrl($shortUrl);
        $sql = 'select * from urls where id="$urlId"';  // либо можно сразу искать по shortUrl, но так дольше для базы
        $result = mysql_query("select * from TABLE NAME where id='$shortUrl'");  
          
        if ($row = mysql_fetch_assoc($result))  
        {  
            //header("location:".$row['url']); 
            return $row['url'];  
        } 
                
        return false;
    }
    
    /** 
     * Конвертирует идентификатор урла в короткую ссылку
     * @param int $urlId Идентификатор урла
     *
     * @return string Короткий url
     */
    private function shortUrlFromId($urlId) 
    {
        $alphabetLength = strlen(self::$alphabet);
     
        $shortUrl = '';
        $n = $urlId;
        while ($n) {
            $shortUrl = self::$alphabet[$n % $alphabetLength].$shortUrl;
            $n = floor($n / $alphabetLength);
        }
        
        return $shortUrl;
    }
    
     /** 
     * Конвертирует короткую ссылку в идентификатор урла
     * 
     * Опциональная функция для оптимизации базы, url можно в базе сразу искать по shortName.
     *
     * @param int $shortURL Короткий url
     *
     * @return string Идентификатор урла
     */
    private function idFromShortUrl($shortUrl)
    {
        $urlId = 0; 
        $alphabetLength = strlen(self::$alphabet);
    
        for ($i=0; $i < strlen($shortUrl); $i++)
        {
            $urlLetterCode = ord($shortUrl[$i]);
            if (ord('a') <= $urlLetterCode && $urlLetterCode <= ord('z'))
              $urlId = $urlId*$alphabetLength + $urlLetterCode - ord('a');
            if (ord('A') <= $urlLetterCode && $urlLetterCode <= ord('Z'))
              $urlId = $urlId*$alphabetLength + $urlLetterCode - ord('A') + 26;
            if (ord('0') <= $urlLetterCode && $urlLetterCode <= ord('9'))
              $urlId = $urlId*$alphabetLength + $urlLetterCode - ord('0') + 52;
        }
        
        return $urlId;
    }
}

// tests
/*$urlShortenerService = new UrlShortenerService;
$shortUrl1 = $urlShortenerService->createShortUrlFromLong("http://yandex.ru");
$shortUrl2 = $urlShortenerService->createShortUrlFromLong("test2");
$shortUrl3 = $urlShortenerService->createShortUrlFromLong("http://citilink.ru/promo/x/");
//$longUrl =   $urlShortenerService->longUrlFromShort($shortUrl1);
echo " urls: $shortUrl1 $shortUrl2 $shortUrl3";*/
